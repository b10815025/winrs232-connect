﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinSupportTool_RS232
{
    public class ColorCodeConverter
    {
        public static void converter(string ansiColor)
        {
            switch (ansiColor)
            {
                case "[0m":
                case "[0;0m":
                    switchColor(colorCode.White);
                    break;
                case "[1;30m":
                    switchColor(colorCode.Black);
                    break;
                case "[1;31m":
                    switchColor(colorCode.Red);
                    break;
                case "[1;32m":
                    switchColor(colorCode.Green);
                    break;
                case "[1;33m":
                    switchColor(colorCode.Yellow);                
                    break;
                case "[1;34m":
                    switchColor(colorCode.Blue);
                    break;
                case "[1;35m":
                    switchColor(colorCode.Magenta);
                    break;
                case "[1;36m":
                    switchColor(colorCode.Cyan);
                    break;
                case "[1;37m":
                    switchColor(colorCode.White);
                    break;
                default:                    
                    break;                    
            }
        }

        private static void switchColor(colorCode color)
        {
            switch (color)
            {
                case colorCode.Black:
                    Console.ForegroundColor = ConsoleColor.Black;
                    break;
                case colorCode.Red:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case colorCode.Green:
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
                case colorCode.Yellow:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
                case colorCode.Blue:
                    Console.ForegroundColor = ConsoleColor.Blue;
                    break;
                case colorCode.Magenta:
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    break;
                case colorCode.Cyan:
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    break;
                case colorCode.White:
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                default:
                    Console.ResetColor();
                    break;
            }            
        }
    }
}
