﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;
using System.Windows.Forms;
using System.Runtime.InteropServices;
namespace WinSupportTool_RS232
{

    public partial class Form1 : Form
    {
        #region API_Called
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool AllocConsole();
        #endregion
        public string portSelectName = string.Empty;

        static SerialPort _serialPort;

        int historyIndex = 0;

        string newMessage = "";
        string newBuffer = "";
        string terminal_buf = "";
        string cmd_temp = "";

        bool colorFlag = false;
        bool refresh = false;
        bool enterFlag = false;

        List<String> commandHistory = new List<String>();

        BackgroundWorker bw = new BackgroundWorker();
        BackgroundWorker bw_terminal = new BackgroundWorker();

        public Form1()
        {
            InitializeComponent();
        }

        private void Link_Click(object sender, EventArgs e)
        {

            _serialPort = new SerialPort(portSelectName); //TODO Change

            _serialPort.BaudRate = 115200;
            _serialPort.Parity = Parity.None;
            _serialPort.StopBits = StopBits.One;
            _serialPort.DataBits = 8;
            _serialPort.Handshake = Handshake.None;
            _serialPort.RtsEnable = Enabled;
            _serialPort.Open();
            Update.Enabled = true;
            bw.DoWork += new DoWorkEventHandler(serialByteRead);
            bw_terminal.DoWork += new DoWorkEventHandler(terminal_Output);
            this.Link.Enabled = false;
        }
        private void terminal_Output(object sender, DoWorkEventArgs e)
        {
            int startIndex = 0;
            string colorCode = string.Empty;
            bool converter_Flag = false;
            for (int i = 0; i < terminal_buf.Length; i++)
            {
                if (terminal_buf[i] == '\n')
                {
                    Console.ResetColor();
                }
                if (terminal_buf[i] == '\u001b')
                {
                    converter_Flag = true;
                    startIndex = i + 1;
                }
                if (!converter_Flag)
                {
                    Console.Write(terminal_buf[i]);
                }
                if (terminal_buf[i] == 'm' && converter_Flag)
                {
                    converter_Flag = false;
                    colorCode = terminal_buf.Substring(startIndex, i - startIndex + 1);
                    ColorCodeConverter.converter(colorCode);
                    terminal_buf = terminal_buf.Remove(startIndex, i - startIndex + 1);
                    i = startIndex - 1;
                }
            }

        }
        public void serialByteRead(object sender, DoWorkEventArgs e)
        {
            if (bw.CancellationPending)
            {
                e.Cancel = true;
            }

            newBuffer += _serialPort.ReadExisting();
            terminal_buf = newBuffer.Clone().ToString();
            if (!bw_terminal.IsBusy)
            {
                bw_terminal.RunWorkerAsync();
            }
            int startIndex = 0;
            string colorCode = string.Empty;
            for (int i = 0; i < newBuffer.Length; i++)
            {
                if (newBuffer[i] == '\u001b')
                {
                    colorFlag = true;
                    startIndex = i + 1;
                }

                if (newBuffer[i] == 'm' && colorFlag)
                {
                    colorCode = newBuffer.Substring(startIndex, i - startIndex + 1);
                    newBuffer = newBuffer.Remove(startIndex, i - startIndex + 1);
                    colorFlag = false;
                    i = startIndex - 1;
                    continue;
                }
            }
            newBuffer = newBuffer.Replace('\u001b', ' ');
            //newBuffer = newBuffer.Replace("[1;34m", " ");
            //newBuffer = newBuffer.Replace("[1;35m", " ");
            //newBuffer = newBuffer.Replace("[1;36m", " ");
            //newBuffer = newBuffer.Replace("[m", " ");
            //newBuffer = newBuffer.Replace("[0m", " ");

            foreach (char c in newBuffer)
            {
                newMessage += c;
            }
            newBuffer = string.Empty;
        }
        private void Update_Tick(object sender, EventArgs e)
        {
            //Start Async
            if (!bw.IsBusy)
            {
                bw.RunWorkerAsync();
            }

            //TODO
            if (newMessage != string.Empty)
            {
                if (refresh)
                {
                    LogView.Text = string.Empty;
                    refreshTimer.Enabled = true;
                    refresh = false;
                }
                LogView.Text += newMessage;
                LogView.Update();
                newMessage = string.Empty;
                LogView.SelectionStart = LogView.Text.Length;
                LogView.ScrollBars = ScrollBars.Vertical;
                LogView.ScrollToCaret();
            }

        }

        private void LinkClose_Click(object sender, EventArgs e)
        {
            _serialPort.Close();
            Link.Enabled = true;
            Update.Enabled = false;
            refresh = false;
            if (bw.IsBusy)
            {
                bw.CancelAsync();
            }
        }

        private void CmdLineInput_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (_serialPort == null) return;


            if (e.KeyChar == 13) //Enter
            {
                if (_serialPort.IsOpen)
                {
                    string command = CmdLineInput.Text;
                    if (command == "clear")
                    {
                        LogView.Text = string.Empty;
                        Console.Clear();
                    }
                    /*Ctrl C ASCII*/
                    if (command == "\\3")
                    {
                        char c = (char)3;
                        _serialPort.Write(c.ToString());
                    }
                    /*Ctrl C ASCII*/
                    if (command != "") commandHistory.Add(command);
                    command += "\n";
                    _serialPort.Write(command);
                    CmdLineInput.Text = String.Empty;
                    enterFlag = true;
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string[] portsName = SerialPort.GetPortNames();
            foreach (string port in portsName)
            {
                portComboBox.Items.Add(port);
            }
        }

        private void portComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            portSelectName = portComboBox.SelectedItem.ToString();
        }

        private void refreshTimer_Tick(object sender, EventArgs e)
        {
            refresh = true;
        }

        private void CmdLineInput_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Up) //Up
            {
                if (enterFlag == true && commandHistory.Count() > 0)
                {
                    cmd_temp = CmdLineInput.Text;
                    enterFlag = false;
                    historyIndex = commandHistory.Count() - 1;
                    CmdLineInput.Text = commandHistory[historyIndex];
                }
                else if (historyIndex > 0)
                {
                    historyIndex--;
                    CmdLineInput.Text = commandHistory[historyIndex];
                }
            }

            if (e.KeyCode == Keys.Down) //Down
            {
                if (enterFlag == true)
                {
                    //Nothing to do 
                }
                else if (enterFlag == false)
                {
                    if (historyIndex + 1 < commandHistory.Count())
                    {
                        historyIndex++;
                        CmdLineInput.Text = commandHistory[historyIndex];
                    }
                    else
                    {
                        historyIndex = commandHistory.Count();
                        CmdLineInput.Text = cmd_temp;
                    }
                }
            }
        }
    }
}
